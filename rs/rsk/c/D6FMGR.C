/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** INTERLEAVED BLOCK HANDLER ****
*
*********************************************************************
*
* FILE NAME:   D6FMGR.C
* 
* DESCRIPTION: Contains routines to handle interleaved blocks.
* 
*    DATE     PROGRAMMER REASON
* --------------------------------------------------------------------
*    8/28/87       L.A. Use OMCM fatal error constant
*
**********************************************************************/
#include <stdio.h>
#include <d6fmgr.in>
#include "objwunit.in"
#include <d7error.in>
#define LEVEL2 2
FREC frecs[MAXFRECS];
extern void fatal_error();
extern unsigned char debug ;

     /* find file record, return index if found -1 otherwise */

find_frec( fname )
char *fname;
{
     int i;
     
     for ( i = 0; i < MAXFRECS; i++ )
          if ( strncmp( frecs[i].fname, fname, 12 ) == 0 )
               break;
     return( i == MAXFRECS ? -1 : i );
}

     /* return index of file record which was set, exit if out of records */

set_frec(fname, fptr)
char *fname;
ObjWHandlet *fptr;
{
     int i ;


#if DEBUG
    if (debug >= LEVEL2 )
       fprintf(stdprn, "NEW set_frec\n\r" );
#endif

    for ( i = 0; *frecs[i].fname != '\0' && i < MAXFRECS; i++ )
#if DEBUG
      if ( debug >= LEVEL2 )
           fprintf(stdprn, "frecs[i].fname == %12s\n\r", frecs[i].fname);
#else
    ;
#endif


    if ( i == MAXFRECS )
     fatal_error (D5_PROC, OUT_OF_INT_RECS);
    
    strncpy( frecs[i].fname, fname, 12 );
    frecs[i].fptr = fptr;
    return( i );
}

free_frec( fname )
char *fname;
{
     int index;
     
     index = find_frec( fname );

#if DEBUG
    if ( debug >= LEVEL2 )
      fprintf(stdprn, "TRYING TO FREE %s at %d\n\r", fname, index);
#endif

     *frecs[index].fname = '\0';
}
/***
main()
{
     static char *names[] = { "file0001.ext", "file0002.ext", "file0003.ext", "file0004.ext", "file0005.ext" }
     int i, index;
     ObjWHandlet *fptr;
     
     fptr = (FILE *)0x1111;
     for (i=0; i<5; i++ )
     {
          index = set_frec(names[i], fptr);
     }
     for( i=0; i< 5; i++ )
     {
          printf( "%s is at %d\n\r", names[i],  find_frec( names[i] ) );
          printf("in this record is: %s, %x\n\r", frecs[i].fname, frecs[i].fptr);
     }
     for (i=0; i<5; i++)
     {
          putchar('\n');
          printf("before free\n\r");
          printf("in this record is: %s, %x\n\r", frecs[i].fname, frecs[i].fptr);
          free_frec( names[i] );
          printf("after free\n\r");
          printf("in this record is: %s, %x\n\r", frecs[i].fname, frecs[i].fptr);
     }

}
***/
