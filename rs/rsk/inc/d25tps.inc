/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*********************************************************************
*
*   File Name: d25tps.inc
*
**********************************************************************
*   Description:  This file contains the keyboard and screen
*                 managing templates/dsects required for driver 3.0
*
*   Revision History:
*
*   Date        Name            Reason
*   -----       ----            -----
*   4/19/85     mel bellar      create
*
*********************************************************************/
    typedef char *OBJ_ID;

/********************** PAINT CONTROL BLOCK TEMPLATE ******************/
       typedef struct obcb {

        unsigned char   seq_num;    /* sequence number of request  */
        unsigned char   source; /* find from rx or on diskette */
        OBJ_ID          obj_id; /* object id to paint          */
                                /* 'U' - upstream 'C' - cache  */
        unsigned char   *obj_ptr;   /* pointer to object in memory */
                                /*  if an upstream object      */
        unsigned short  obj_cnt;    /* size of object to paint     */
        unsigned char   cache;      /* YES/NO -tells interpreter to cache or*/
                                    /*  not                                 */
/*HG*/    char                status;        /* status on requested object */
    } OBCB;

/******************* TRANSMIT CONTROL BLOCK TEMPLATE ******************/
       typedef struct xcb {
        
        unsigned char   req_code;   /* request code                */
        unsigned char   seq_num;    /* DIA sequence # of request/  */
                                    /*  response                   */
        unsigned char   flags1;     /* DIA flag byte               */
/* Driver 7 - C. H. 2/12/87                                */
        DIAPARM          *diablk;    /* pointer to DIA cntrl info blk */
        
        unsigned char   *tx_ptr;    /* pointer to transmit data    */
        unsigned short  tx_cnt;     /* data length                 */
    } XCB;

/******************* RECEIVE CONTROL BLOCK TEMPLATE ******************/
       typedef struct rcb {

        unsigned char   req_code;   /* request code                */
        unsigned char   seq_num;    /* DIA sequence # of request/  */ 
                                /* response                    */ 
        unsigned char   flags1; /* DIA flag byte (retransmit)  */ 
        unsigned char   *tx_ptr;    /* pointer to transmit data    */ 
        unsigned short  tx_cnt; /*  data length(for retransmit)*/ 
        unsigned char   *rx_ptr;    /* pointer to transmit data    */ 
        unsigned short  rx_cnt; /*  data length(for retransmit)*/ 
        unsigned char   skip;   /* set to 1 if this RCB is     */ 
                                /*  received out of order      */
        unsigned char   num_blks;   /* number of blocks in multiblk*/ 
        unsigned char   cur_blk;    /* current block in multi-block*/ 
    } RCB;

/******************* QUEUE CONTROL BLOCK TEMPLATE *********************/
       typedef struct qcb {

        unsigned char   *q_top;         /* beginning of FIFO           */
        unsigned char   *q_end;         /* end of FIFO                 */
        unsigned char   *q_in;          /* pointer to next input slot  */
        unsigned char   *q_out;         /* pointer to next output slot */
        unsigned char   q_step;         /* length of a queue entry     */
    } QCB;

/*************** DIRECTORY CONTROL BLOCK TEMPLATE *********************/
       typedef struct dcb {
 
/*HG*/  char            obj_id[14];     /* object id                   */
        unsigned char   type;           /* stage or cached             */
    } DCB;

/*********************** KEYWORD TEMPLATE ***************************/
    typedef struct kcb {
/*HG*/  char        key[14];
/*HG*/  char        obj_id[14];
    } KCB;

/* HG BEG */
    typedef struct acb  {
        char            adkey[14];          /* ad keyword                */
        char            ldr_obj[14];        /* leader ad                 */ 
        char            fup_obj[14];        /* follow up ad              */ 
        } ACB;

/* HG END */

/* HG BEG */
    typedef struct adkey {          /* ad list record structure  */
        char            adkey[14];  /* for type casting purposes */
    }ADLST;
/* HG END */
/******************* OBJECT MANAGER CONTROL BLOCK TEMPLATE **************/
/*
     NB: obj_id and version must be contiguous, without padding.
*/
typedef struct omcb {
      unsigned char obj_id[13];
          unsigned char version[2];     /* LH 10/13/87 */
/*
     Note: would've preferred to do this --

          OVersionBytest version;

     But that causes even-byte alignment, which is inadmissible here.
*/
          char type;
    } OMCB;
/******************* USER INFORMATION CONTROL BLOCK TEMPLATE ************/
typedef struct ucb
       {
        unsigned char   userid[8];
        unsigned char   password[7];
        unsigned char   profile;
        } UCB;


/******************* DIRECTORY INFORMATION CONTROL BLOCK ************/
/*HG*/ typedef struct dib
       {
        char *filename;         /* directory filename */
        char *errmessg;         /* string for purposes of error messages */
        char *dirptr;           /* memory ptr to directory */
/*HG*/ } DIB;

/******************  MESSAGE CONTROL BLOCK *********************/
       typedef struct mcb
       {
          unsigned char   *rcv_msg;     /* ptr to received message  */
     unsigned short  rcv_msg_len;  /* data count for total message */
     unsigned char  msg_status;    /* error indication:      */
     unsigned char  seq_num;  /* OK, NOT_FOUND, NOT_ARRIVED */
                         /* 0 , 1        , 2             */
/* Driver 7 - C. H. 2/12/87                               */
     DIAPARM        *rcv_diaptr;   /* ptr to received DIA info */


       } MCB;


 typedef struct ext2type
     {
          char      *ext;                         /* file extension i.e. .MAP
          char      type;                         /* type byte of object id
     } EXT2TYPE;
