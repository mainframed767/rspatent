/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*         **** LOGICAL KEY TO FUNCTION AND DISPLAY EVENT ****
*
*********************************************************************
*
* FILE NAME:   lk_evt.in
*
* DESCRIPTION:
*         Service manager include file containing:
*              Array of logical keys mapped to
*              function event and display event.
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*    28 APRIL '86       A.J.M.           ORIGINAL
*
**********************************************************************/

KEY_TO_FUNCTION_MAP key_to_functions[] = {

     NO_EVENT,NO_EVENT,  /* NULL_KEY         0    */
     ADD_TEXT,ECHO_TEXT, /* TEXT_KEY         1    */

     ADD_TEXT,ECHO_TEXT, /* A_KEY       2    */
     ADD_TEXT,ECHO_TEXT, /* B_KEY       3    */
     ADD_TEXT,ECHO_TEXT, /* C_KEY       4    */
     ADD_TEXT,ECHO_TEXT, /* D_KEY       5    */
     ADD_TEXT,ECHO_TEXT, /* E_KEY       6    */
     ADD_TEXT,ECHO_TEXT, /* F_KEY       7    */
     ADD_TEXT,ECHO_TEXT, /* G_KEY       8    */
     ADD_TEXT,ECHO_TEXT, /* H_KEY       9    */
     ADD_TEXT,ECHO_TEXT, /* I_KEY       10   */
     ADD_TEXT,ECHO_TEXT, /* J_KEY       11   */
     ADD_TEXT,ECHO_TEXT, /* K_KEY       12   */
     ADD_TEXT,ECHO_TEXT, /* L_KEY       13   */
     ADD_TEXT,ECHO_TEXT, /* M_KEY       14   */
     ADD_TEXT,ECHO_TEXT, /* N_KEY       15   */
     ADD_TEXT,ECHO_TEXT, /* O_KEY       16   */
     ADD_TEXT,ECHO_TEXT, /* P_KEY       17   */
     ADD_TEXT,ECHO_TEXT, /* Q_KEY       18   */
     ADD_TEXT,ECHO_TEXT, /* R_KEY       19   */
     ADD_TEXT,ECHO_TEXT, /* S_KEY       20   */
     ADD_TEXT,ECHO_TEXT, /* T_KEY       21   */
     ADD_TEXT,ECHO_TEXT, /* U_KEY       22   */
     ADD_TEXT,ECHO_TEXT, /* V_KEY       23   */
     ADD_TEXT,ECHO_TEXT, /* W_KEY       24   */
     ADD_TEXT,ECHO_TEXT, /* X_KEY       25   */
     ADD_TEXT,ECHO_TEXT, /* Y_KEY       26   */
     ADD_TEXT,ECHO_TEXT, /* Z_KEY       27   */

                    /* Lower case alphas          */

     ADD_TEXT,ECHO_TEXT, /* LA_KEY      28   */
     ADD_TEXT,ECHO_TEXT, /* LB_KEY      29   */
     ADD_TEXT,ECHO_TEXT, /* LC_KEY      30   */
     ADD_TEXT,ECHO_TEXT, /* LD_KEY      31   */
     ADD_TEXT,ECHO_TEXT, /* LE_KEY      32   */
     ADD_TEXT,ECHO_TEXT, /* LF_KEY      33   */
     ADD_TEXT,ECHO_TEXT, /* LG_KEY      34   */
     ADD_TEXT,ECHO_TEXT, /* LH_KEY      35   */
     ADD_TEXT,ECHO_TEXT, /* LI_KEY      36   */
     ADD_TEXT,ECHO_TEXT, /* LJ_KEY      37   */
     ADD_TEXT,ECHO_TEXT, /* LK_KEY      38   */
     ADD_TEXT,ECHO_TEXT, /* LL_KEY      39   */
     ADD_TEXT,ECHO_TEXT, /* LM_KEY      40   */
     ADD_TEXT,ECHO_TEXT, /* LN_KEY      41   */
     ADD_TEXT,ECHO_TEXT, /* LO_KEY      42   */
     ADD_TEXT,ECHO_TEXT, /* LP_KEY      43   */
     ADD_TEXT,ECHO_TEXT, /* LQ_KEY      44   */
     ADD_TEXT,ECHO_TEXT, /* LR_KEY      45   */
     ADD_TEXT,ECHO_TEXT, /* LS_KEY      46   */
     ADD_TEXT,ECHO_TEXT, /* LT_KEY      47   */
     ADD_TEXT,ECHO_TEXT, /* LU_KEY      48   */
     ADD_TEXT,ECHO_TEXT, /* LV_KEY      49   */
     ADD_TEXT,ECHO_TEXT, /* LW_KEY      50   */
     ADD_TEXT,ECHO_TEXT, /* LX_KEY      51   */
     ADD_TEXT,ECHO_TEXT, /* LY_KEY      52   */
     ADD_TEXT,ECHO_TEXT, /* LZ_KEY      53   */

     ADD_TEXT,ECHO_TEXT, /* N0_KEY      54   */
     ADD_TEXT,ECHO_TEXT, /* N1_KEY      55   */
     ADD_TEXT,ECHO_TEXT, /* N2_KEY      56   */
     ADD_TEXT,ECHO_TEXT, /* N3_KEY      57   */
     ADD_TEXT,ECHO_TEXT, /* N4_KEY      58   */
     ADD_TEXT,ECHO_TEXT, /* N5_KEY      59   */
     ADD_TEXT,ECHO_TEXT, /* N6_KEY      60   */
     ADD_TEXT,ECHO_TEXT, /* N7_KEY      61   */
     ADD_TEXT,ECHO_TEXT, /* N8_KEY      62   */
     ADD_TEXT,ECHO_TEXT, /* N9_KEY      63   */

     BACKSPACE,DESTRUCTIVE_BACK, /* BACKSPACE_KEY 64   */
     TOGGLE_INSERT,0,    /* INSERT_KEY       65   */
     DELETE_TEXT,FORWARD_DELETE, /* DELETE_KEY    66   */

     CURSOR,MOVE_CURSOR_UP,   /* CURSOR_UP_KEY    67   */
     CURSOR,MOVE_CURSOR_DOWN,/* CURSOR_DOWN_KEY   68   */
     CURSOR,MOVE_CURSOR_LEFT,/* CURSOR_LEFT_KEY   69   */
     CURSOR,MOVE_CURSOR_RIGHT,/* CURSOR_RIGHT_KEY 70   */

     FIELD_END,TAB_LEFT, /* TAB_LEFT_KEY          71   */
     FIELD_END,TAB_RIGHT,     /* TAB_RIGHT_KEY    72   */
     FIELD_END,TAB_UP,   /* TAB_UP_KEY       73   */
     FIELD_END,TAB_DOWN, /* TAB_DOWN_KEY,    74   */
     FIELD_END,TAB_FORWARD,   /* TAB_FORWARD_KEY  75   */
     FIELD_END,TAB_BACK, /* TAB_BACK_KEY          76   */

     FIELD_END,HOME,          /* HOME_KEY         77   */
     FIELD_END,LAST_FIELD,    /* END_KEY          78   */

     NO_EVENT,SCROLL_FIELD_UP,/* SCROLL_UP_KEY    79   */
     NO_EVENT,SCROLL_FIELD_DOWN,/* SCROLL_DOWN_KEY     80   */

     SET_CLOSE_WINDOW,NO_EVENT,/* CLOSE_WINDOW_KEY     81   */
     PAGE_END,NO_EVENT,  /* COMMIT_KEY       82   */
     ADD_TEXT,ECHO_TEXT, /* HELP_KEY         83   */

     NEXT_PAGE,NO_EVENT, /* NEXT_KEY         84   */
     BACK_PAGE,NO_EVENT, /* BACK_KEY         85   */
     PATH,NO_EVENT,      /* PATH_KEY         86   */
     SCAN,NO_EVENT,      /* SCAN_KEY         87   */

     ADD_TEXT,ECHO_TEXT, /* SPACE_BAR        88   */
     PAGE_HELP,NO_EVENT, /* PAGE_HELP_KEY        89    */
     ACTION,NO_EVENT,    /* ACTION_KEY       90   */
     VIEWPATH,NO_EVENT,  /* VIEWPATH_KEY          91   */
     GUIDE,NO_EVENT,          /* GUIDE_KEY        92   */
     INDEX,NO_EVENT,          /* INDEX_KEY        93   */
     MARK,NO_EVENT,      /* KEEP_KEY         94   */
     PREVIOUS_MENU,NO_EVENT,  /* PREVIOUS_MENU_KEY     95   */
     UNDO,NO_EVENT,           /* UNDO_KEY         96   */
    SRNPRT,NO_EVENT     /* PRT_KEY      97  */
};
