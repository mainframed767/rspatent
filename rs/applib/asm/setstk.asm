;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;* the msc library routines have stack check built in (you
;* can suppress it in your own routines). They use a variable
;* set to the original stack range. If you create cp88 tasks,
;* the stack is different for each one and you will get an error.
;* use this routine to set the stack to zero and prevent the
;* internal error.
;*
_TEXT     SEGMENT  BYTE PUBLIC 'CODE'
_TEXT     ENDS
CONST     SEGMENT  WORD PUBLIC 'CONST'
CONST     ENDS
_BSS SEGMENT  WORD PUBLIC 'BSS'
_BSS ENDS
_DATA     SEGMENT  WORD PUBLIC 'DATA'
_DATA     ENDS
DGROUP    GROUP     CONST,    _BSS,     _DATA
     ASSUME    CS: _TEXT, DS: DGROUP, SS: DGROUP, ES:DGROUP
PUBLIC    _setstk
_DATA     SEGMENT
IFDEF     @BIGMODEL
@AB  EQU  6
ELSE
@AB  EQU  4
ENDIF
     extrn     STKHQQ:word
_DATA     ENDS
_TEXT        SEGMENT
IFDEF     @BIGMODEL
_setstk proc   far
ELSE
_setstk proc   near
ENDIF
     push bp          ;save bp
     mov  bp,sp            ;get parameter pointer
     mov  ax,@AB[bp]       ;get new STKHQQ value
     mov  STKHQQ,ax   ;set STKHQQ to new value
     pop  bp          ;reset bp
     ret              ;return to caller
_setstk endp
_TEXT     ENDS
     END
;**********************************************************************/
;*     END SETSTK.ASM                              */
;**********************************************************************/